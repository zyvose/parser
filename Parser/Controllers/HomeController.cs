﻿using AngleSharp;
using AngleSharp.Extensions;
using AngleSharp.Parser.Html;
using Parser.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Parser.Controllers
{
    public class HomeController : Controller
    {
        ParserContext db = new ParserContext();
        HtmlParser parser = new HtmlParser();
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<Shop> shops = db.Shops;
            
            ViewBag.Shops = shops;
            return View();
        }
        
        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult> ParseCategories(int id)
        {
            
            if (id == 1)
            {
                
                var config = Configuration.Default.WithDefaultLoader();
                var address = "https://www.eldorado.ru/cat/";

                var document =await BrowsingContext.New(config).OpenAsync(address);
                var ListCategoriesSelector = document.QuerySelectorAll("h2.new-catalog-block__main-title-text");
                var catalogBlocks = document.QuerySelectorAll("div.new-catalog-block");
                List<string> categories = new List<string>();
                List<string> subcategories1 = new List<string>();
                List<string> subcategories2 = new List<string>();
                



                foreach (var item in catalogBlocks)
                {
                    int categoryChecker = 0;
                    var categoryName = item.QuerySelector("h2.new-catalog-block__main-title-text").Text();
                    Category currentCategory = new Category { Shop_Id = id, Name = categoryName };
                    foreach (var category in db.Categories)
                        if(category.Shop_Id==currentCategory.Shop_Id&&category.Name==currentCategory.Name)
                        {
                            categoryChecker++;
                        }
                    if (categoryChecker == 0)
                    {
                        db.Categories.Add(currentCategory);
                        db.SaveChanges();
                    }
                    var Subcategories1 = item.QuerySelectorAll("div.new-catalog-block__col");
                    foreach (var subcat1 in Subcategories1)
                    {
                        int subcategory1Checker = 0;
                        var subcategory1Name = subcat1.QuerySelector("div.new-catalog-block__title a").Text();
                        Subcategory1 currentSubcategory1 = new Subcategory1 { Category_Id = currentCategory.CategoryId, Name = subcategory1Name };
                        foreach (var subcategory1 in db.Subcategries1)
                            if (subcategory1.Category_Id == currentSubcategory1.Category_Id && subcategory1.Name == currentSubcategory1.Name)
                            {
                                subcategory1Checker++;
                            }
                        if (subcategory1Checker == 0)
                        {
                            db.Subcategries1.Add(currentSubcategory1);
                            db.SaveChanges();
                        }
                        var Subcategories2 = subcat1.QuerySelectorAll("div.new-catalog-block__elem");
                        foreach (var subcat2 in Subcategories2)
                        {
                            int subcategory2Checker = 0;
                            var Subcategory2Name = subcat2.QuerySelector("a.new-catalog-block__link").Text();
                            Subcategory2 currentSubcategory2 = new Subcategory2 { Subcategory1Id = currentSubcategory1.Subcategory1Id, Name = Subcategory2Name };
                            foreach (var subcategory2 in db.Subcategries2)
                                if (subcategory2.Subcategory1Id == currentSubcategory2.Subcategory1Id && subcategory2.Name == currentSubcategory2.Name)
                                {
                                    subcategory2Checker++;
                                }
                            if (subcategory2Checker == 0)
                            {
                                db.Subcategries2.Add(currentSubcategory2);
                                db.SaveChanges();
                            }
                        }


                    }



                }


                



               
            }
            if (id == 2)
            {
                var config = Configuration.Default.WithDefaultLoader();
                var address = "http://www.e-kvadrat.ru/catalog/";

                var document = await BrowsingContext.New(config).OpenAsync(address);
                
                var catalog = document.QuerySelector("div.catalogue_list");
                var catalogItems = catalog.QuerySelectorAll("div.item");
                foreach (var item in catalogItems)
                {
                    int categoryChecker = 0;

                    var categoryName = item.QuerySelector("li a").Text();
                    Category currentCategory = new Category { Shop_Id = id, Name = categoryName };
                    foreach (var category in db.Categories)
                        if (category.Shop_Id == currentCategory.Shop_Id && category.Name == currentCategory.Name)
                        {
                            categoryChecker++;
                        }
                    if (categoryChecker == 0)
                    {
                        db.Categories.Add(currentCategory);
                        db.SaveChanges();
                    }
                    var Subcategories1 = item.QuerySelectorAll("li ul a");
                    foreach (var subcat1 in Subcategories1)
                    {
                        int subcategory1Checker = 0;
                        var subcategory1Name = subcat1.Text();
                        Subcategory1 currentSubcategory1 = new Subcategory1 { Category_Id = currentCategory.CategoryId, Name = subcategory1Name };
                        foreach (var subcategory1 in db.Subcategries1)
                            if (subcategory1.Category_Id == currentSubcategory1.Category_Id && subcategory1.Name == currentSubcategory1.Name)
                            {
                                subcategory1Checker++;
                            }
                        if (subcategory1Checker == 0)
                        {
                            db.Subcategries1.Add(currentSubcategory1);
                            db.SaveChanges();
                        }


                    }



                }
            }
            ViewBag.X = "Категории товаров получены";
            return View();
        }
        
        [HttpPost]
        public ActionResult ParseCategories()
        {
            return Redirect("/Home/Index");
        }
        [HttpGet]
        public ActionResult DbTables()
        {
            IEnumerable<Category> categories = db.Categories;
            ViewBag.categories = categories;
            IEnumerable<Subcategory1> subcategories1 = db.Subcategries1;
            ViewBag.subcategories1 = subcategories1;
            IEnumerable<Subcategory2> subcategories2 = db.Subcategries2;
            ViewBag.subcategories2 = subcategories2;
            return View();
        }
        
    }
    
}