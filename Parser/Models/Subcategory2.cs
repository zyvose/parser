﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.Models
{
    public class Subcategory2
    {
        public int Subcategory2Id { get; set; }
        public string Name { get; set; }
        public int Subcategory1Id { get; set; }
    }
}