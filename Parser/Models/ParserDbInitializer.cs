﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Parser.Models
{
    public class ParserDbInitializer: DropCreateDatabaseIfModelChanges<ParserContext>
    {
        protected override void Seed(ParserContext db)
        {
            db.Shops.Add(new Shop { Name = "Эльдорадо"});
            db.Shops.Add(new Shop { Name = "Квадрат"});
            

            base.Seed(db);
        }
    }
}