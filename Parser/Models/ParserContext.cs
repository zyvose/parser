﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Parser.Models
{
    public class ParserContext:DbContext
    {
        public ParserContext()
            : base("ParserContext")
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled = true;
        }
        public DbSet<Shop> Shops{ get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory1> Subcategries1 { get; set; }
        public DbSet<Subcategory2> Subcategries2 { get; set; }
    }
}