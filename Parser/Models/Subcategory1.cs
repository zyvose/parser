﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.Models
{
    public class Subcategory1
    {
        public int Subcategory1Id { get; set; }
        public string Name { get; set; }
        public int Category_Id { get; set; }
        
    }
}